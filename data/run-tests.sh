#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

real_skip='44-2 55-2'

usage()
{
	cat <<'EOUSAGE'
Usage:	run-tests /path/to/program /path/to/datadir
EOUSAGE
}

run_test()
{
	local program="$1" datadir="$2" test="$3"

	echo "Running the $test test for $program"

	echo '- trivial'
	"$program" "$test" -- "$datadir/${test%-*}-trivial-input.txt" > "$tempf"
	cat -- "$tempf"
	diff -u -- "$datadir/$test-trivial-output.txt" "$tempf"

	if [ -f "$datadir/$test-trivial-2-output.txt" ]; then
		echo '- trivial too'
		"$program" "$test" -- "$datadir/${test%-*}-trivial-2-input.txt" > "$tempf"
		cat -- "$tempf"
		diff -u -- "$datadir/$test-trivial-2-output.txt" "$tempf"
	fi

	if [ "${real_skip#*$test}" = "$real_skip" ]; then
		echo '- real'
		"$program" "$test" -- "$datadir/${test%-*}-real-input.txt" > "$tempf"
		cat -- "$tempf"
		diff -u -- "$datadir/$test-real-output.txt" "$tempf"
	else
		echo '- real: skipped'
	fi
}

run_tests()
{
	local program="$1" datadir="$2"
	local line

	for line in $($program implemented); do
		case "$line" in
			[0-2][0-9]-[12])
				run_test "$program" "$datadir" "$line"
				;;

			*)
				echo "Unexpected output from '$program implemented': $line" 1>&2
				exit 1
				;;
		esac
	done
}

if [ "$#" -ne 2 ]; then
	usage 1>&2
	exit 1
fi

tempf="$(mktemp run-aoc-tests.txt.XXXXXX)"
# shellcheck disable=SC2064  # Doing this on purpose.
trap "rm -f -- '$tempf'" EXIT HUP INT TERM QUIT

run_tests "$1" "$2"
