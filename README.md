<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Peter Pentchev's Advent of Code 2023 solvers

If you are reading this, then you already know what this repo is about.
But just in case you don't, take a look at the [Advent of Code][aoc] website.

## Testing the Rust implementation

Build the Rust program, then run `data/run-tests.sh` with the path to
the compiled program and the path to the data/ directory,
e.g. something like:

    cargo build
    data/run-tests.sh target/debug/aoc-2022 data

[aoc]: https://adventofcode.com/
