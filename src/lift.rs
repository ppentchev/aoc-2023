// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Handle things related to the gondola lift.

use std::ops::RangeInclusive;

use crate::defs::{BuildDecimal, Config, Error};

use anyhow::Context;

/// A single character in the schematics.
#[derive(Debug)]
enum SChar {
    /// An empty space in the schematics.
    Empty,

    /// A digit, part of a part number.
    Digit(i32),

    /// A symbol.
    Symbol(char),
}

impl TryFrom<char> for SChar {
    type Error = Error;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        if let Some(digit) = value.to_digit(10) {
            Ok(Self::Digit(
                digit
                    .try_into()
                    .with_context(|| format!("Could not convert {digit} to i32"))
                    .map_err(Error::Process)?,
            ))
        } else if value == '.' {
            Ok(Self::Empty)
        } else {
            Ok(Self::Symbol(value))
        }
    }
}

/// An engine part with its coordinates.
#[derive(Debug, PartialEq, Eq)]
pub struct Part {
    /// The part number.
    number: i32,

    /// The X coordinate range to look for possible symbols.
    range_x: RangeInclusive<i32>,

    /// The Y coordinate range to look for possible symbols.
    range_y: RangeInclusive<i32>,
}

impl Part {
    /// The part number.
    #[inline]
    #[must_use]
    pub const fn number(&self) -> i32 {
        self.number
    }

    /// The X coordinate range to look for possible symbols.
    #[inline]
    #[must_use]
    pub const fn range_x(&self) -> &RangeInclusive<i32> {
        &self.range_x
    }

    /// The Y coordinate range to look for possible symbols.
    #[inline]
    #[must_use]
    pub const fn range_y(&self) -> &RangeInclusive<i32> {
        &self.range_y
    }

    /// Consume this part and replace the part number.
    #[inline]
    #[must_use]
    pub const fn with_number(self, number: i32) -> Self {
        Self { number, ..self }
    }

    /// Consume this part and extend the X range rightwards.
    #[inline]
    #[must_use]
    pub const fn extend_x(self, next_x: i32) -> Self {
        Self {
            range_x: (*self.range_x.start())..=next_x,
            ..self
        }
    }
}

/// A symbol in the engine schematics with its coordinates.
#[derive(Debug, PartialEq, Eq)]
pub struct Symbol {
    /// The symbol itself.
    sym: char,

    /// The X coordinate.
    x: i32,

    /// The Y coordinate.
    y: i32,
}

impl Symbol {
    /// The symbol itself.
    #[inline]
    #[must_use]
    pub const fn sym(&self) -> char {
        self.sym
    }

    /// The X coordinate.
    #[inline]
    #[must_use]
    pub const fn x(&self) -> i32 {
        self.x
    }

    /// The Y coordinate.
    #[inline]
    #[must_use]
    pub const fn y(&self) -> i32 {
        self.y
    }
}

/// A line containing part numbers and symbols.
#[derive(Debug)]
pub struct SLine {
    /// The parts defined on this line.
    parts: Vec<Part>,

    /// The symbols defined on this line.
    symbols: Vec<Symbol>,
}

impl SLine {
    /// The parts defined on this line.
    #[inline]
    #[must_use]
    pub fn parts(&self) -> &[Part] {
        &self.parts
    }

    /// The symbols defined on this line.
    #[inline]
    #[must_use]
    pub fn symbols(&self) -> &[Symbol] {
        &self.symbols
    }
}

/// The full schematics of the engine.
#[derive(Debug)]
pub struct Schematics {
    /// The parts comprising the engine.
    parts: Vec<Part>,

    /// The symbols scattered all over the schematics.
    symbols: Vec<Symbol>,
}

impl Schematics {
    /// The parts comprising the engine.
    #[inline]
    #[must_use]
    pub fn parts(&self) -> &[Part] {
        &self.parts
    }

    /// The symbols scattered all over the schematics.
    #[inline]
    #[must_use]
    pub fn symbols(&self) -> &[Symbol] {
        &self.symbols
    }

    /// Gather all the parts and symbols into a [`Schematics`] object.
    #[inline]
    #[must_use]
    pub fn from_lines<I: Iterator<Item = SLine>>(lines: I) -> Self {
        let mut parts = vec![];
        let mut symbols = vec![];
        for line in lines {
            parts.extend(line.parts);
            symbols.extend(line.symbols);
        }

        Self { parts, symbols }
    }
}

/// The intermediate state kept while parsing a schematics line.
type SLineState = (Vec<Part>, Vec<Symbol>, Option<Part>);

/// Parse a whole schematics line containing part numbers and symbols.
///
/// # Errors
///
/// [`Error::Value`] if an input line cannot be parsed.
/// [`Error::Process`] if a small number cannot be converted between integer types.
fn parse_sline(line_y: i32, line: &str) -> Result<SLine, Error> {
    let (mut parts, symbols, current_part) = line.chars().enumerate().try_fold(
        (vec![], vec![], None),
        |(mut parts, mut symbols, mut current_part): SLineState,
         (uidx, chr)|
         -> Result<SLineState, Error> {
            let idx = i32::try_from(uidx)
                .with_context(|| format!("Could not convert the {uidx} line position to i32"))
                .map_err(Error::Process)?;
            let next_x = idx
                .checked_add(1)
                .with_context(|| format!("Could not get a right X coordinate for {idx}"))
                .map_err(Error::Process)?;
            let schar: SChar = chr.try_into()?;

            match current_part.take() {
                None => match schar {
                    SChar::Empty => Ok((parts, symbols, None)),
                    SChar::Symbol(sym) => {
                        symbols.push(Symbol {
                            sym,
                            x: idx,
                            y: line_y,
                        });
                        Ok((parts, symbols, None))
                    }
                    SChar::Digit(digit) => {
                        let prev_x = idx
                            .checked_sub(1)
                            .with_context(|| format!("Could not get a left X coordinate for {idx}"))
                            .map_err(Error::Process)?;
                        let prev_y = line_y
                            .checked_sub(1)
                            .with_context(|| {
                                format!("Could not get an up Y coordinate for {line_y}")
                            })
                            .map_err(Error::Process)?;
                        let next_y = line_y
                            .checked_add(1)
                            .with_context(|| {
                                format!("Could not get a down Y coordinate for {line_y}")
                            })
                            .map_err(Error::Process)?;
                        let part = Part {
                            number: digit,
                            range_x: prev_x..=next_x,
                            range_y: prev_y..=next_y,
                        };
                        Ok((parts, symbols, Some(part)))
                    }
                },
                Some(current) => match schar {
                    SChar::Empty => {
                        parts.push(current);
                        Ok((parts, symbols, None))
                    }
                    SChar::Digit(digit) => {
                        let number = current.number().add_digit(digit)?;
                        Ok((
                            parts,
                            symbols,
                            Some(current.with_number(number).extend_x(next_x)),
                        ))
                    }
                    SChar::Symbol(sym) => {
                        parts.push(current);
                        symbols.push(Symbol {
                            sym,
                            x: idx,
                            y: line_y,
                        });
                        Ok((parts, symbols, None))
                    }
                },
            }
        },
    )?;

    if let Some(current) = current_part {
        parts.push(current);
    }
    Ok(SLine { parts, symbols })
}

/// Read in the engine schematics from a file.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn read_schem_lines<C: Config>(cfg: &C) -> Result<Vec<SLine>, Error> {
    cfg.read_lines()?
        .enumerate()
        .map(|(uline_y, line_res)| -> Result<SLine, Error> {
            let line_y = i32::try_from(uline_y)
                .with_context(|| format!("Could not convert the {uline_y} line index to i32"))
                .map_err(Error::Process)?;
            let line = line_res?;
            parse_sline(line_y, &line)
                .with_context(|| format!("Could not parse the {line:?} input line"))
                .map_err(Error::Value)
        })
        .collect()
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use rstest::rstest;

    use super::{Part, Symbol};

    #[rstest]
    #[case("1.", vec![Part { number: 1, range_x: -1_i32..=1_i32, range_y: 2_i32..=4_i32 }], vec![])]
    #[case("1", vec![Part { number: 1, range_x: -1_i32..=1_i32, range_y: 2_i32..=4_i32 }], vec![])]
    #[case("42.", vec![Part { number: 42, range_x: -1_i32..=2_i32, range_y: 2_i32..=4_i32 }], vec![])]
    #[case("42", vec![Part { number: 42, range_x: -1_i32..=2_i32, range_y: 2_i32..=4_i32 }], vec![])]
    #[case(
        "42.616.",
        vec![
            Part { number: 42, range_x: -1_i32..=2_i32, range_y: 2_i32..=4_i32 },
            Part { number: 616, range_x: 2_i32..=6_i32, range_y: 2_i32..=4_i32 },
        ],
        vec![],
    )]
    #[case(
        "42.616",
        vec![
            Part { number: 42, range_x: -1_i32..=2_i32, range_y: 2_i32..=4_i32 },
            Part { number: 616, range_x: 2_i32..=6_i32, range_y: 2_i32..=4_i32 },
        ],
        vec![],
    )]
    #[case(
        ".2.616.",
        vec![
            Part { number: 2, range_x: 0_i32..=2_i32, range_y: 2_i32..=4_i32 },
            Part { number: 616, range_x: 2_i32..=6_i32, range_y: 2_i32..=4_i32 },
        ],
        vec![],
    )]
    #[case(
        ".2.616",
        vec![
            Part { number: 2, range_x: 0_i32..=2_i32, range_y: 2_i32..=4_i32 },
            Part { number: 616, range_x: 2_i32..=6_i32, range_y: 2_i32..=4_i32 },
        ],
        vec![],
    )]
    #[case("#", vec![], vec![Symbol { sym: '#', x: 0, y: 3 }])]
    #[case(
        "1#",
        vec![Part { number: 1, range_x: -1_i32..=1_i32, range_y: 2_i32..=4_i32}],
        vec![Symbol { sym: '#', x: 1, y: 3 }],
    )]
    fn test_line(#[case] line: &str, #[case] parts: Vec<Part>, #[case] symbols: Vec<Symbol>) {
        let sline = super::parse_sline(3, line).unwrap();
        assert_eq!((sline.parts(), sline.symbols()), (&*parts, &*symbols));
    }
}
