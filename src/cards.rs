// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the food-related tests.

use std::cmp::{Ord, Ordering, PartialOrd};

use anyhow::{anyhow, Context};
use itertools::Itertools;
use nom::{
    character::complete::{one_of, space1, u32 as c_u32},
    combinator::all_consuming,
    multi::count,
    sequence::{preceded, tuple},
    IResult,
};

use crate::defs::{Config, Error};

/// A single card.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[non_exhaustive]
pub enum Card {
    /// 2
    Two,

    /// 3
    Three,

    /// 4,
    Four,

    /// 5
    Five,

    /// 6
    Six,

    /// 7
    Seven,

    /// 8
    Eight,

    /// 9
    Nine,

    /// 10
    Ten,

    /// J
    Jack,

    /// Q
    Queen,

    /// K
    King,

    /// A
    Ace,
}

impl Card {
    /// 2
    const TWO: char = '2';

    /// 3
    const THREE: char = '3';

    /// 4
    const FOUR: char = '4';

    /// 5
    const FIVE: char = '5';

    /// 6
    const SIX: char = '6';

    /// 7
    const SEVEN: char = '7';

    /// 8
    const EIGHT: char = '8';

    /// 9
    const NINE: char = '9';

    /// 10
    const TEN: char = 'T';

    /// J
    const JACK: char = 'J';

    /// Q
    const QUEEN: char = 'Q';

    /// K
    const KING: char = 'K';

    /// A
    const ACE: char = 'A';

    /// The numeric value of a card, useful for sorting.
    #[inline]
    #[must_use]
    pub const fn num_value(&self) -> u32 {
        match *self {
            Self::Two => 2,
            Self::Three => 3,
            Self::Four => 4,
            Self::Five => 5,
            Self::Six => 6,
            Self::Seven => 7,
            Self::Eight => 8,
            Self::Nine => 9,
            Self::Ten => 10,
            Self::Jack => 11,
            Self::Queen => 12,
            Self::King => 13,
            Self::Ace => 14,
        }
    }
}

impl TryFrom<char> for Card {
    type Error = Error;

    #[inline]
    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            Self::TWO => Ok(Self::Two),
            Self::THREE => Ok(Self::Three),
            Self::FOUR => Ok(Self::Four),
            Self::FIVE => Ok(Self::Five),
            Self::SIX => Ok(Self::Six),
            Self::SEVEN => Ok(Self::Seven),
            Self::EIGHT => Ok(Self::Eight),
            Self::NINE => Ok(Self::Nine),
            Self::TEN => Ok(Self::Ten),
            Self::JACK => Ok(Self::Jack),
            Self::QUEEN => Ok(Self::Queen),
            Self::KING => Ok(Self::King),
            Self::ACE => Ok(Self::Ace),
            other => Err(Error::Value(anyhow!(
                "Unrecognized card character {other:?}"
            ))),
        }
    }
}

impl Ord for Card {
    #[inline]
    #[must_use]
    fn cmp(&self, other: &Self) -> Ordering {
        self.num_value().cmp(&other.num_value())
    }
}

impl PartialOrd for Card {
    #[inline]
    #[must_use]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// The type of a hand.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[allow(clippy::exhaustive_enums)]
pub enum Type {
    /// Five of a kind.
    FiveOfAKind,

    /// Four of a kind.
    FourOfAKind,

    /// Full house.
    FullHouse,

    /// Three of a kind.
    ThreeOfAKind,

    /// Two pair.
    TwoPair,

    /// One pair.
    OnePair,

    /// High card.
    HighCard,
}

impl Type {
    /// Determine the type of a hand.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] if something goes weirdly wrong.
    #[inline]
    pub fn from_cards(cards: &[Card]) -> Result<Self, Error> {
        let counts = cards
            .iter()
            .counts()
            .into_values()
            .sorted()
            .collect::<Vec<_>>();
        match *counts {
            [5] => Ok(Self::FiveOfAKind),
            [1, 4] => Ok(Self::FourOfAKind),
            [2, 3] => Ok(Self::FullHouse),
            [1, 1, 3] => Ok(Self::ThreeOfAKind),
            [1, 2, 2] => Ok(Self::TwoPair),
            [1, 1, 1, 2] => Ok(Self::OnePair),
            [1, 1, 1, 1, 1] => Ok(Self::HighCard),
            _ => Err(Error::Process(anyhow!(
                "What kind of combo even is {counts:?}"
            ))),
        }
    }

    /// The numeric weight of the type, suitable for sorting.
    #[inline]
    #[must_use]
    pub const fn num_value(&self) -> u32 {
        match *self {
            Self::FiveOfAKind => 6,
            Self::FourOfAKind => 5,
            Self::FullHouse => 4,
            Self::ThreeOfAKind => 3,
            Self::TwoPair => 2,
            Self::OnePair => 1,
            Self::HighCard => 0,
        }
    }
}

impl Ord for Type {
    #[inline]
    #[must_use]
    fn cmp(&self, other: &Self) -> Ordering {
        self.num_value().cmp(&other.num_value())
    }
}

impl PartialOrd for Type {
    #[inline]
    #[must_use]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// A hand as found on the input line.
#[derive(Debug)]
struct HandParsed {
    /// The five cards.
    cards: Vec<char>,
}

/// A hand of cards.
#[derive(Debug, PartialEq, Eq)]
pub struct Hand {
    /// The five cards.
    cards: Vec<Card>,

    /// The type of the hand.
    hand_type: Type,
}

impl Hand {
    /// The five cards.
    #[inline]
    #[must_use]
    pub fn cards(&self) -> &[Card] {
        &self.cards
    }

    /// The type of the hand.
    #[inline]
    #[must_use]
    pub const fn hand_type(&self) -> Type {
        self.hand_type
    }

    /// Validate and parse a hand from the input line.
    ///
    /// # Errors
    ///
    /// [`Error::Value`] if an input line cannot be parsed.
    /// [`Error::Process`] if something goes really, really weird.
    fn from_parsed(parsed: HandParsed) -> Result<Self, Error> {
        if parsed.cards.len() != 5 {
            return Err(Error::Value(anyhow!(
                "Expected five cards, got {cards:?}",
                cards = parsed.cards
            )));
        }
        let cards = parsed
            .cards
            .into_iter()
            .map(Card::try_from)
            .collect::<Result<Vec<_>, _>>()?;
        let hand_type = Type::from_cards(&cards)?;
        Ok(Self { cards, hand_type })
    }
}

impl Ord for Hand {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.hand_type.cmp(&other.hand_type).then_with(|| {
            self.cards
                .iter()
                .zip(other.cards.iter())
                .fold(Ordering::Equal, |acc, (our_card, their_card)| {
                    acc.then_with(|| our_card.cmp(their_card))
                })
        })
    }
}

impl PartialOrd for Hand {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// A hand along with its corresponding bid as found on the input line.
#[derive(Debug)]
struct HandAndBidParsed {
    /// The hand of five cards.
    hand: HandParsed,

    /// The bid amount.
    bid: u32,
}

/// A hand along with its corresponding bid.
#[derive(Debug, PartialEq, Eq)]
pub struct HandAndBid {
    /// The hand of five cards.
    hand: Hand,

    /// The bid amount.
    bid: u32,
}

impl HandAndBid {
    /// The hand of five cards.
    #[inline]
    #[must_use]
    pub const fn hand(&self) -> &Hand {
        &self.hand
    }

    /// The bid amount.
    #[inline]
    #[must_use]
    pub const fn bid(&self) -> u32 {
        self.bid
    }

    /// Validate and parse a hand and a bid from the input line.
    ///
    /// # Errors
    ///
    /// [`Error::Value`] if an input line cannot be parsed.
    fn from_parsed(parsed: HandAndBidParsed) -> Result<Self, Error> {
        Ok(Self {
            hand: Hand::from_parsed(parsed.hand)?,
            bid: parsed.bid,
        })
    }
}

impl Ord for HandAndBid {
    #[inline]
    fn cmp(&self, other: &Self) -> Ordering {
        self.hand.cmp(&other.hand)
    }
}

impl PartialOrd for HandAndBid {
    #[inline]
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

/// Parse a single character corresponding to a card.
fn p_card(input: &str) -> IResult<&str, char> {
    one_of("AKQJT98765432")(input)
}

/// Parse an input line into an almost-hand-and-bid structure.
fn p_hand_and_bid(input: &str) -> IResult<&str, HandAndBidParsed> {
    let (r_input, (cards, bid)) =
        all_consuming(tuple((count(p_card, 5), preceded(space1, c_u32))))(input)?;
    Ok((
        r_input,
        HandAndBidParsed {
            hand: HandParsed { cards },
            bid,
        },
    ))
}

/// Parse and validate an input line into a hand and a bid.
///
/// # Errors
///
/// [`Error::Value`] if an input line cannot be parsed.
fn parse_hand(line: &str) -> Result<HandAndBid, Error> {
    let (_, handbid) = p_hand_and_bid(line)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .with_context(|| format!("Could not parse the {line:?} input line"))
        .map_err(Error::Value)?;
    HandAndBid::from_parsed(handbid)
}

/// Read the hands and their corresponding bids from the input file.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn read_hands_and_bids<C: Config>(cfg: &C) -> Result<Vec<HandAndBid>, Error> {
    cfg.read_lines()?
        .map(|line_res| parse_hand(&line_res?))
        .collect::<Result<Vec<_>, _>>()
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use rstest::rstest;

    use super::{Card, Hand, HandAndBid, Type};

    #[rstest]
    #[case(
        "23456 10",
        HandAndBid {
            hand: Hand {
                cards: vec![Card::Two, Card::Three, Card::Four, Card::Five, Card::Six],
                hand_type: Type::HighCard,
            },
            bid: 10,
        }
    )]
    #[case(
        "789TJ 21",
        HandAndBid {
            hand: Hand {
                cards: vec![Card::Seven, Card::Eight, Card::Nine, Card::Ten, Card::Jack],
                hand_type: Type::HighCard,
            },
            bid: 21,
        }
    )]
    #[case(
        "QKAAK 32",
        HandAndBid {
            hand: Hand {
                cards: vec![Card::Queen, Card::King, Card::Ace, Card::Ace, Card::King],
                hand_type: Type::TwoPair,
            },
            bid: 32,
        }
    )]
    fn test_parse_hand(#[case] line: &str, #[case] exp: HandAndBid) {
        assert_eq!(super::parse_hand(line).unwrap(), exp);
    }

    #[rstest]
    #[case("AAAAA 6", Type::FiveOfAKind)]
    #[case("AAAA2 2", Type::FourOfAKind)]
    #[case("55535 1", Type::FourOfAKind)]
    #[case("44K44 2", Type::FourOfAKind)]
    #[case("76777 2", Type::FourOfAKind)]
    #[case("42222 2", Type::FourOfAKind)]
    #[case("JTJTJ 2", Type::FullHouse)]
    #[case("JTJ9J 2", Type::ThreeOfAKind)]
    #[case("JTKKJ 2", Type::TwoPair)]
    #[case("KTJ9J 2", Type::OnePair)]
    #[case("JTK97 2", Type::HighCard)]
    fn test_parse_type(#[case] line: &str, #[case] exp: Type) {
        assert_eq!(super::parse_hand(line).unwrap().hand().hand_type(), exp);
    }
}
