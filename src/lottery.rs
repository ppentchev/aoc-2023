// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the lottery-related tests.

use std::collections::HashSet;
use std::iter;

use anyhow::Context;
use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{space1, u32 as c_u32},
    combinator::all_consuming,
    multi::many1,
    sequence::{delimited, preceded, terminated, tuple},
    IResult,
};

use crate::defs::{Config, Error};

/// Something to carry over to the next step...
#[derive(Debug)]
pub struct CarryOver {
    /// The number of rounds left.
    rounds: u32,

    /// The number of copies to add for the following cards.
    copies: u32,
}

impl CarryOver {
    /// Construct a [`CarryOver`] object from a number of rounds and number of copies.
    #[inline]
    #[must_use]
    const fn new(rounds: u32, copies: u32) -> Self {
        Self { rounds, copies }
    }

    /// Decrease the number of rounds, let the caller know if this one fizzles out.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] on failure to subtract small integers.
    fn decrease(self) -> Result<Option<Self>, Error> {
        let dec = self
            .rounds
            .checked_sub(1)
            .with_context(|| format!("Could not decrease the number of rounds for {self:?}"))
            .map_err(Error::Process)?;
        Ok((dec != 0).then_some(Self {
            rounds: dec,
            ..self
        }))
    }
}

/// A lottery card: winning numbers, our numbers.
#[derive(Debug)]
pub struct Card {
    /// The list of winning numbers.
    winning: HashSet<u32>,

    /// The list of our numbers.
    ours: HashSet<u32>,
}

impl Card {
    /// Calculate the number of matches as an [`u32`] value.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] if something goes wrong with converting small numbers.
    fn matches(&self) -> Result<u32, Error> {
        let u_count = self.ours.intersection(&self.winning).count();
        u32::try_from(u_count)
            .context("Could not convert {u_count} to u32")
            .map_err(Error::Process)
    }

    /// Calculate our winning score.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
    #[inline]
    pub fn score(&self) -> Result<u32, Error> {
        match self.matches()? {
            0 => Ok(0),
            count => {
                let shift = count
                    .checked_sub(1)
                    .with_context(|| {
                        format!("Could not subtract 1 from {count} to get the shift count")
                    })
                    .map_err(Error::Process)?;
                1_u32
                    .checked_shl(shift)
                    .with_context(|| format!("Could not shift 1 left {count} times"))
                    .map_err(Error::Process)
            }
        }
    }

    /// Calculate the number of copies affected by previous cards, decide how to affect the next cards.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] on failure to add, subtract, or multiply small integers.
    #[allow(clippy::missing_inline_in_public_items)]
    pub fn copies_with_carry(&self, carry: Vec<CarryOver>) -> Result<(u32, Vec<CarryOver>), Error> {
        let score = self.matches()?;
        let copies = carry
            .iter()
            .map(|item| item.copies)
            .chain(iter::once(1))
            .sum();

        let carry_over = {
            let mut carry_over = carry
                .into_iter()
                .map(CarryOver::decrease)
                .flatten_ok()
                .collect::<Result<Vec<_>, _>>()?;
            if score > 0 {
                carry_over.push(CarryOver::new(score, copies));
            }
            carry_over
        };
        Ok((copies, carry_over))
    }
}

/// Parse a full card definition line.
fn p_card_line(input: &str) -> IResult<&str, Card> {
    let (r_input, (_id, winning, ours)) = all_consuming(tuple((
        delimited(tuple((tag("Card"), space1)), c_u32, tag(":")),
        terminated(many1(preceded(space1, c_u32)), tag(" |")),
        many1(preceded(space1, c_u32)),
    )))(input)?;
    Ok((
        r_input,
        Card {
            winning: winning.into_iter().collect(),
            ours: ours.into_iter().collect(),
        },
    ))
}

/// Read the lottery cards in.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn read_cards<C: Config>(cfg: &C) -> Result<Vec<Card>, Error> {
    cfg.read_lines()?
        .map(|line_res| {
            let line = line_res?;
            let (_, card) = p_card_line(&line)
                .map_err(|err| err.map_input(ToOwned::to_owned))
                .with_context(|| format!("Could not parse the {line:?} input line"))
                .map_err(Error::Value)?;
            Ok(card)
        })
        .collect::<Result<Vec<_>, _>>()
}
