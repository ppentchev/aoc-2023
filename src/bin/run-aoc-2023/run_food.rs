// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the food-related tests.

use anyhow::anyhow;

use aoc_2023::defs::{Config, Error};
use aoc_2023::food::{self, SeedsMode};

/// Find the seed with the lowest location.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
fn run_test<C: Config>(cfg: &C, mode: SeedsMode) -> Result<String, Error> {
    let maps = food::read_maps(cfg, mode)?;
    let (seeds, unmaps) = maps.take_seeds();
    unmaps
        .recursive_map("seed", "location", seeds)?
        .into_iter()
        .min_by(food::cmp_range_inclusive)
        .map_or_else(
            || {
                Err(Error::Internal(anyhow!(
                    "There should be at least one seed in the parsed map"
                )))
            },
            |range| Ok(range.start().to_string()),
        )
}

/// Find the seed with the lowest location, single indices on the initial seeds line.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_05_1<C: Config>(cfg: &C) -> Result<String, Error> {
    run_test(cfg, SeedsMode::Single)
}

/// Find the seed with the lowest location, ranges on the initial seeds line.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_05_2<C: Config>(cfg: &C) -> Result<String, Error> {
    run_test(cfg, SeedsMode::Ranges)
}
