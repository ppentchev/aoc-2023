#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! A possible implementation of the 2023 Advent of Code puzzle solver.

use std::collections::HashMap;
use std::env;
use std::io;
use std::path::{Path, PathBuf};

use anyhow::{bail, Context, Result};
use itertools::Itertools;
use tracing::Level;

use aoc_2023::defs::{Config as AocConfig, Error};

mod run_aim;
mod run_cards;
mod run_cubes;
mod run_food;
mod run_lift;
mod run_lottery;
mod run_race;

/// Runtime configuration for the Advent of Code solver.
#[derive(Debug)]
struct Config {
    /// The input filename.
    filename: PathBuf,
}

impl AocConfig for Config {
    fn filename(&self) -> &Path {
        &self.filename
    }
}

/// Run the specified test with the specified filename.
///
/// # Errors
///
/// Die on string (UTF-8) parsing errors.
fn run<F>(tag: &str, testf: F, filename: &str) -> Result<String>
where
    F: FnOnce(&Config) -> Result<String, Error>,
{
    {
        let level = if filename.contains("trivial") {
            Level::TRACE
        } else {
            Level::INFO
        };
        let sub = tracing_subscriber::fmt()
            .with_max_level(level)
            .with_writer(io::stderr)
            .finish();
        #[allow(clippy::absolute_paths)]
        tracing::subscriber::set_global_default(sub)
            .context("Could not initialize the tracing logger")?;
    }
    let cfg = Config {
        filename: filename
            .try_into()
            .context("Could not parse the input filename")?,
    };
    testf(&cfg).with_context(|| format!("{tag} failed"))
}

/// The runner for a single problem.
type Handler = dyn Fn(&Config) -> Result<String, Error>;

/// All the problem handler runners.
const TEST_HANDLERS: [(&str, &Handler); 13] = [
    ("01-1", &run_aim::test_01_1),
    ("01-2", &run_aim::test_01_2),
    ("02-1", &run_cubes::test_02_1),
    ("02-2", &run_cubes::test_02_2),
    ("03-1", &run_lift::test_03_1),
    ("03-2", &run_lift::test_03_2),
    ("04-1", &run_lottery::test_04_1),
    ("04-2", &run_lottery::test_04_2),
    ("05-1", &run_food::test_05_1),
    ("05-2", &run_food::test_05_2),
    ("06-1", &run_race::test_06_1),
    ("06-2", &run_race::test_06_2),
    ("07-1", &run_cards::test_07_1),
];

// OVERRIDE: This is the main purpose of the test runner.
#[allow(clippy::print_stdout)]
fn main() -> Result<()> {
    let args: Vec<_> = env::args().skip(1).collect();
    let handlers = TEST_HANDLERS.into_iter().collect::<HashMap<_, _>>();
    println!(
        "{}",
        match args
            .iter()
            .filter_map(|arg| (arg != "--").then_some(arg.as_str()))
            .collect::<Vec<_>>()[..]
        {
            ["implemented"] => TEST_HANDLERS
                .into_iter()
                .map(|(test_name, _)| test_name)
                .join("\n"),
            [test_name, filename] => run(
                test_name,
                handlers
                    .get(test_name)
                    .with_context(|| format!("Invalid test name '{test_name}'"))
                    .map_err(Error::Process)?,
                filename
            )?,
            _ => bail!("Usage: aoc-2023 implemented | aoc-2023 test-id input-file"),
        }
    );
    Ok(())
}
