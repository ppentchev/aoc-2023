// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the race-related tests.

use anyhow::{anyhow, Context};
use tracing::trace;

use aoc_2023::defs::{Config, Error};
use aoc_2023::race::{self, Race};

/// Find the number of ways we can win a single race.
///
/// # Errors
///
/// [`Error::Process`] on failure to perform calculations.
#[allow(clippy::as_conversions)]
#[allow(clippy::cast_sign_loss)]
#[allow(clippy::cast_precision_loss)]
#[allow(clippy::cast_possible_truncation)]
#[allow(clippy::float_arithmetic)]
fn calc_count(race: &Race) -> Result<u64, Error> {
    let time = race.time();
    let distance = race.distance();
    let discr = time
        .checked_mul(time)
        .with_context(|| format!("Could not square race time {time}"))
        .map_err(Error::Process)?
        .checked_sub(
            distance
                .checked_mul(4)
                .with_context(|| format!("Could not multiply the race distance {distance} by 4"))
                .map_err(Error::Process)?,
        )
        .with_context(|| format!("Could not calculate {time} ^ 2 - 4 * {distance}"))
        .map_err(Error::Process)?;
    let sq_discr = (discr as f64).sqrt();
    if !sq_discr.is_normal() {
        return Err(Error::Process(anyhow!(
            "Could not calculate the square root of {discr}"
        )));
    }

    let n_min = ((time as f64) - sq_discr) / 2.0_f64;
    let n_min_ceil = n_min.ceil();
    if !n_min_ceil.is_normal() {
        return Err(Error::Process(anyhow!(
            "Could not calculate the smaller root for {race:?}"
        )));
    }
    let u_min = n_min_ceil as u64;
    let excl_min = u64::from((n_min_ceil - n_min) < 0.000_000_1_f64);
    trace!(n_min);
    trace!(u_min);
    trace!(excl_min);

    let n_max = ((time as f64) + sq_discr) / 2.0_f64;
    let n_max_floor = n_max.floor();
    if !n_max_floor.is_normal() {
        return Err(Error::Process(anyhow!(
            "Could not calculate the larger root for {race:?}"
        )));
    }
    let u_max = n_max_floor as u64;
    let excl_max = u64::from((n_max - n_max_floor) < 0.000_000_1_f64);
    trace!(n_max);
    trace!(u_max);
    trace!(excl_max);

    u_max
        .checked_sub(u_min)
        .with_context(|| {
            format!("Could not subtract the smaller root {u_min} from the larger one {u_max}")
        })
        .map_err(Error::Process)?
        .checked_add(1)
        .with_context(|| format!("Could not add 1 to the difference between {u_max} and {u_min}"))
        .map_err(Error::Process)?
        .checked_sub(excl_min)
        .with_context(|| format!("Could not subtract the exclude-min value {excl_min}"))
        .map_err(Error::Process)?
        .checked_sub(excl_max)
        .with_context(|| format!("Could not subtract the exclude-max value {excl_max}"))
        .map_err(Error::Process)
}

/// Find the number of ways we can win the races.
///
/// # Errors
///
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
fn run_test(races: &[Race]) -> Result<String, Error> {
    races
        .iter()
        .map(calc_count)
        .try_fold(1, |total, count_res| -> Result<u64, Error> {
            let count = count_res?;
            total
                .checked_mul(count)
                .with_context(|| {
                    format!("Could not multiply the total {total} by the {count:?} count")
                })
                .map_err(Error::Process)
        })
        .map(|res| res.to_string())
}

/// Find the number of ways we can win all the races.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_06_1<C: Config>(cfg: &C) -> Result<String, Error> {
    run_test(&race::read_races(cfg, false)?)
}

/// Find the number of ways we can win the single big race.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_06_2<C: Config>(cfg: &C) -> Result<String, Error> {
    run_test(&race::read_races(cfg, true)?)
}
