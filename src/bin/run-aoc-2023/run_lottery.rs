// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the lottery-related tests.

use anyhow::Context;

use aoc_2023::defs::{Config, Error};
use aoc_2023::lottery::{self, CarryOver};

/// Get the sum of the lottery winnings.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_04_1<C: Config>(cfg: &C) -> Result<String, Error> {
    Ok(lottery::read_cards(cfg)?
        .into_iter()
        .map(|card| card.score())
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .sum::<u32>()
        .to_string())
}

/// Get the total number of scratchcards won.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_04_2<C: Config>(cfg: &C) -> Result<String, Error> {
    Ok(lottery::read_cards(cfg)?
        .into_iter()
        .try_fold(
            (0, vec![]),
            |(tsum, carry), card| -> Result<(u32, Vec<CarryOver>), Error> {
                let (copies, carry_over) = card.copies_with_carry(carry)?;
                let tsum_next = tsum
                    .checked_add(copies)
                    .with_context(|| format!("Could not add {copies} cards to the {tsum} total"))
                    .map_err(Error::Process)?;
                Ok((tsum_next, carry_over))
            },
        )?
        .0
        .to_string())
}
