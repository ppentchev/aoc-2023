// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the food-related tests.

use anyhow::Context;
use itertools::Itertools;

use aoc_2023::cards;
use aoc_2023::defs::{Config, Error};

/// Find the total winnings of the specified hands.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
fn run_test<C: Config>(cfg: &C) -> Result<String, Error> {
    let hands = cards::read_hands_and_bids(cfg)?;
    Ok(hands
        .into_iter()
        .sorted_unstable()
        .enumerate()
        .map(|(idx, handbid)| {
            u32::try_from(
                idx.checked_add(1)
                    .with_context(|| "Could not add 1 to the {idx} card index")
                    .map_err(Error::Process)?,
            )
            .context("Could not convert the card rank to u32")
            .map_err(Error::Process)?
            .checked_mul(handbid.bid())
            .with_context(|| {
                format!(
                    "Could not multiply the card rank by the {bid} bid",
                    bid = handbid.bid()
                )
            })
            .map_err(Error::Process)
        })
        .fold_ok(Ok(0_u32), |acc_res, product| {
            acc_res.and_then(|acc| {
                acc.checked_add(product)
                    .with_context(|| {
                        format!("Could not add the {product} product to the {acc} total")
                    })
                    .map_err(Error::Process)
            })
        })??
        .to_string())
}

/// Find the total winnings of the specified hands.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_07_1<C: Config>(cfg: &C) -> Result<String, Error> {
    run_test(cfg)
}
