// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the gondola-lift-related tests.

use anyhow::Context;

use aoc_2023::defs::{Config, Error};
use aoc_2023::lift::{self, Schematics};

/// Get the sum of the part numbers.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_03_1<C: Config>(cfg: &C) -> Result<String, Error> {
    let schem = Schematics::from_lines(lift::read_schem_lines(cfg)?.into_iter());
    Ok(schem
        .parts()
        .iter()
        .filter_map(|part| {
            schem
                .symbols()
                .iter()
                .any(|sym| part.range_x().contains(&sym.x()) && part.range_y().contains(&sym.y()))
                .then_some(part.number())
        })
        .sum::<i32>()
        .to_string())
}

/// Get the sum of the gear ratios.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_03_2<C: Config>(cfg: &C) -> Result<String, Error> {
    let schem = Schematics::from_lines(lift::read_schem_lines(cfg)?.into_iter());
    Ok(schem
        .symbols()
        .iter()
        .filter(|sym| sym.sym() == '*')
        .filter_map(|sym| {
            let parts = schem
                .parts()
                .iter()
                .filter_map(|part| {
                    (part.range_x().contains(&sym.x()) && part.range_y().contains(&sym.y()))
                        .then_some(part.number())
                })
                .collect::<Vec<_>>();
            match parts[..] {
                [first, second] => Some(
                    first
                        .checked_mul(second)
                        .with_context(|| format!("Could not multiply {first} by {second}"))
                        .map_err(Error::Process),
                ),
                _ => None,
            }
        })
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .sum::<i32>()
        .to_string())
}
