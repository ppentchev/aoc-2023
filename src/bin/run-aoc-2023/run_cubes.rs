// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the cubes-related tests.

use anyhow::Context;

use aoc_2023::cubes::{self, DRAW_THRESHOLD};
use aoc_2023::defs::{Config, Error};

/// Determine which games are possible with a predefined set of cubes.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_02_1<C: Config>(cfg: &C) -> Result<String, Error> {
    cubes::read_games(cfg)?
        .into_iter()
        .filter_map(|game| {
            let possible = game
                .draws()
                .iter()
                .all(|draw| draw.possible(&DRAW_THRESHOLD));
            possible.then_some(game.id())
        })
        .try_fold(0, |acc: u32, id| {
            acc.checked_add(id)
                .with_context(|| format!("Could not add game ID {id} to the {acc} total"))
                .map_err(Error::Process)
        })
        .map(|value| value.to_string())
}

/// Get the total power of all the minimum sets.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_02_2<C: Config>(cfg: &C) -> Result<String, Error> {
    cubes::read_games(cfg)?
        .into_iter()
        .map(|game| game.power())
        .collect::<Result<Vec<_>, _>>()?
        .into_iter()
        .try_fold(0, |acc: u32, id| {
            acc.checked_add(id)
                .with_context(|| format!("Could not add game ID {id} to the {acc} total"))
                .map_err(Error::Process)
        })
        .map(|value| value.to_string())
}
