// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the trebuchet-related tests.

use anyhow::Context;

use aoc_2023::aim;
use aoc_2023::defs::{Config, Error};

/// Get the calibration value out of a series of lines that may or may not contain longform values.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
fn run_test_01<C: Config>(cfg: &C, longform: bool) -> Result<String, Error> {
    Ok(cfg
        .read_lines()?
        .try_fold(0, |acc: u32, line_res| {
            let line = line_res?;
            let value = aim::parse_calibration(&line, longform)?;
            acc.checked_add(value)
                .with_context(|| {
                    format!("Could not add the {value} calibration value to the {acc} total")
                })
                .map_err(Error::Process)
        })?
        .to_string())
}

/// Get the calibration value out of a series of lines.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_01_1<C: Config>(cfg: &C) -> Result<String, Error> {
    run_test_01(cfg, false)
}

/// Get the calibration value out of a series of lines that may contain longform values.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn test_01_2<C: Config>(cfg: &C) -> Result<String, Error> {
    run_test_01(cfg, true)
}
