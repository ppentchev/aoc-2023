// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the race-related tests.

use std::fs;

use anyhow::{anyhow, Context};
use nom::{
    bytes::complete::tag,
    character::complete::{space1, u64 as c_u64},
    combinator::all_consuming,
    multi::many1,
    sequence::{delimited, preceded, tuple},
    IResult,
};

use crate::defs::{Config, Error};

/// The definition of a single race we need to run.
#[derive(Debug)]
pub struct Race {
    /// The time we must fit into.
    time: u64,

    /// The distance we need to beat.
    distance: u64,
}

impl Race {
    /// The time we must fit into.
    #[inline]
    #[must_use]
    pub const fn time(&self) -> u64 {
        self.time
    }

    /// The distance we need to beat.
    #[inline]
    #[must_use]
    pub const fn distance(&self) -> u64 {
        self.distance
    }
}

/// Parse the full data about the races.
fn p_races(input: &str) -> IResult<&str, (Vec<u64>, Vec<u64>)> {
    let (r_input, (times, distances)) = all_consuming(tuple((
        delimited(tag("Time:"), many1(preceded(space1, c_u64)), tag("\n")),
        delimited(tag("Distance:"), many1(preceded(space1, c_u64)), tag("\n")),
    )))(input)?;
    Ok((r_input, (times, distances)))
}

/// Read the race definitions from the input file.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
#[allow(clippy::missing_inline_in_public_items)]
pub fn read_races<C: Config>(cfg: &C, single_race: bool) -> Result<Vec<Race>, Error> {
    let contents = {
        let full_contents = fs::read_to_string(cfg.filename())
            .context("Could not read the input file")
            .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
        if single_race {
            full_contents.replace(' ', "").replace(':', ": ")
        } else {
            full_contents
        }
    };
    let (_, (times, distances)) = p_races(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(Error::Value)?;
    if times.len() != distances.len() {
        return Err(Error::Value(anyhow!(
            "Times count {c_times} does not match distances count {c_distances}",
            c_times = times.len(),
            c_distances = distances.len()
        )));
    }
    Ok(times
        .into_iter()
        .zip(distances)
        .map(|(time, distance)| Race { time, distance })
        .collect())
}
