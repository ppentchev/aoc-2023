// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Common definitions for the Advent of Code 2023 solvers.

use std::fmt::{Debug, Display};
use std::fs::File;
use std::io::{BufRead, BufReader, Lines};
use std::path::Path;

use anyhow::{Context, Error as AnyError};
use thiserror::Error;

/// Errors that occur during processing stuff.
#[derive(Debug, Error)]
#[non_exhaustive]
#[allow(clippy::error_impl_error)]
pub enum Error {
    /// Could not load an input file.
    #[error("Could not load input data from the {0} file")]
    Load(String, #[source] AnyError),

    /// Something went really, really wrong...
    #[error("aoc-2023 internal error")]
    Internal(#[source] AnyError),

    /// Something went wrong during processing the data.
    #[error("Could not process the input data")]
    Process(#[source] AnyError),

    /// Unexpected data in the input files.
    #[error("Unexpected input data")]
    Value(#[source] AnyError),
}

/// Read lines from a file, convert I/O errors to our own ones.
#[derive(Debug)]
pub struct ConfigReadLines<BR: BufRead> {
    /// The name of the input file to read.
    cfname: String,

    /// The reader to read the lines from.
    lines: Lines<BR>,
}

impl<BR: BufRead> ConfigReadLines<BR> {
    /// Construct a new [`ConfigReadLines`] object wrapping the specified reader.
    const fn new(cfname: String, lines: Lines<BR>) -> Self {
        Self { cfname, lines }
    }
}

impl<BR: BufRead> Iterator for ConfigReadLines<BR> {
    type Item = Result<String, Error>;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.lines.next().map(|line_res| {
            line_res
                .context("Could not read a line")
                .map_err(|err| Error::Load(self.cfname.clone(), err))
        })
    }
}

/// Common configuration for the tests.
pub trait Config: Debug {
    /// The input filename to process.
    fn filename(&self) -> &Path;

    /// Iterate over the lines read from the input file.
    ///
    /// # Errors
    ///
    /// [`Error::Load`] on I/O errors.
    #[inline]
    fn read_lines(&self) -> Result<ConfigReadLines<BufReader<File>>, Error> {
        let cfname = format!("{}", self.filename().display());
        match File::open(self.filename()).context("Could not read the input file") {
            Err(err) => Err(Error::Load(cfname, err)),
            Ok(infile) => Ok(ConfigReadLines::new(cfname, BufReader::new(infile).lines())),
        }
    }
}

/// Build a decimal number digit by digit.
pub trait BuildDecimal: Sized + Display {
    /// Add a digit to a decimal number.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] if adding or multiplying small numbers fails.
    fn add_digit(self, digit: Self) -> Result<Self, Error>;
}

impl BuildDecimal for i32 {
    #[inline]
    fn add_digit(self, digit: Self) -> Result<Self, Error> {
        self.checked_mul(10)
            .with_context(|| format!("Could not multiply {self} by 10"))
            .map_err(Error::Process)?
            .checked_add(digit)
            .with_context(|| format!("Could not add {digit} to {self} * 10"))
            .map_err(Error::Process)
    }
}

impl BuildDecimal for u32 {
    #[inline]
    fn add_digit(self, digit: Self) -> Result<Self, Error> {
        self.checked_mul(10)
            .with_context(|| format!("Could not multiply {self} by 10"))
            .map_err(Error::Process)?
            .checked_add(digit)
            .with_context(|| format!("Could not add {digit} to {self} * 10"))
            .map_err(Error::Process)
    }
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use rstest::rstest;

    use super::{BuildDecimal, Error};

    #[rstest]
    #[case("1", 1_i32)]
    #[case("42", 42_i32)]
    #[case("616", 616_i32)]
    fn test_add_digit_i32(#[case] value: &str, #[case] exp: i32) {
        let res = value.chars().fold(0_i32, |acc, chr| {
            acc.add_digit(chr.to_digit(10).unwrap().try_into().unwrap())
                .unwrap()
        });
        assert_eq!(res, exp);
    }

    #[test]
    fn test_add_digit_i32_fail() {
        let value = u32::MAX.to_string();
        let res = value.chars().try_fold(0_i32, |acc, chr| {
            acc.add_digit(chr.to_digit(10).unwrap().try_into().unwrap())
        });
        assert!(matches!(res, Err(Error::Process(_))));
    }

    #[rstest]
    #[case("1", 1_u32)]
    #[case("42", 42_u32)]
    #[case("616", 616_u32)]
    fn test_add_digit_u32(#[case] value: &str, #[case] exp: u32) {
        let res = value.chars().fold(0_u32, |acc, chr| {
            acc.add_digit(chr.to_digit(10).unwrap()).unwrap()
        });
        assert_eq!(res, exp);
    }

    #[test]
    fn test_add_digit_u32_fail() {
        let value = u64::MAX.to_string();
        let res = value
            .chars()
            .try_fold(0_u32, |acc, chr| acc.add_digit(chr.to_digit(10).unwrap()));
        assert!(matches!(res, Err(Error::Process(_))));
    }
}
