#![deny(missing_docs)]
#![deny(clippy::missing_docs_in_private_items)]
// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Helper functions and classes for the Advent of Code 2023 puzzle solver.

pub mod aim;
pub mod cards;
pub mod cubes;
pub mod defs;
pub mod food;
pub mod lift;
pub mod lottery;
pub mod race;
