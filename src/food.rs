// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Run the food-related tests.

use std::cmp::Ordering;
use std::collections::HashMap;
use std::fs;
use std::ops::RangeInclusive;

use anyhow::{anyhow, Context};
use itertools::Itertools;
use nom::{
    bytes::complete::tag,
    character::complete::{alpha1, space1, u32 as c_u32},
    combinator::all_consuming,
    multi::many1,
    sequence::{delimited, preceded, terminated, tuple},
    IResult,
};

use crate::defs::{Config, Error};

/// Should we treat the initial seeds line as individual seeds or ranges?
#[derive(Debug, Clone, Copy)]
#[allow(clippy::exhaustive_enums)]
pub enum SeedsMode {
    /// Two consecutive numbers specify a range.
    Ranges,

    /// Each number is an index of a seed.
    Single,
}

/// The ranges we process.
pub type ResourceRange = RangeInclusive<u32>;

/// A mapping range.
#[derive(Debug)]
pub struct MapRange {
    /// The first value to map from.
    start_from: u32,

    /// The first value to map to.
    start_to: u32,

    /// The number of identifiers to map.
    count: u32,

    /// The last value to map to.
    end_to: u32,

    /// The source range to check against.
    range: ResourceRange,
}

impl MapRange {
    /// The first value to map from.
    #[inline]
    #[must_use]
    pub const fn start_from(&self) -> u32 {
        self.start_from
    }

    /// The first value to map to.
    #[inline]
    #[must_use]
    pub const fn start_to(&self) -> u32 {
        self.start_to
    }

    /// The number of identifiers to map.
    #[inline]
    #[must_use]
    pub const fn count(&self) -> u32 {
        self.count
    }

    /// The last value to map to.
    #[inline]
    #[must_use]
    pub const fn end_to(&self) -> u32 {
        self.end_to
    }

    /// The starting range to check against.
    #[inline]
    #[must_use]
    pub const fn range(&self) -> &ResourceRange {
        &self.range
    }

    /// Remap a single value that has already been found to be in the range.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] on failure to find the value within the range or
    /// to perform addition and subtraction.
    #[inline]
    pub fn remap_in_range(&self, res_from: &str, res_to: &str, value: u32) -> Result<u32, Error> {
        let offset = value
            .checked_sub(self.start_from)
            .with_context(|| {
                format!("{res_from}-{res_to}: could not find the offset of {value} within {self:?}")
            })
            .map_err(Error::Process)?;
        let res = self
            .start_to
            .checked_add(offset)
            .with_context(|| {
                format!(
                    "{res_from}-{res_to}: could not add {offset} to the {start_to} start-to value",
                    start_to = self.start_to
                )
            })
            .map_err(Error::Process)?;
        Ok(res)
    }

    /// Remap a single value.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] on failure to find the value within the range or
    /// to perform addition and subtraction.
    #[inline]
    pub fn remap(&self, res_from: &str, res_to: &str, value: u32) -> Result<u32, Error> {
        if self.range.contains(&value) {
            self.remap_in_range(res_from, res_to, value)
        } else {
            Ok(value)
        }
    }
}

/// A mapping range as found directly in the input file.
#[derive(Debug)]
pub struct MapRangeParsed {
    /// The starting point to map from.
    start_from: u32,

    /// The starting point to map to.
    start_to: u32,

    /// The number of identifiers to map.
    count: u32,
}

impl TryFrom<MapRangeParsed> for MapRange {
    type Error = Error;

    #[inline]
    fn try_from(prange: MapRangeParsed) -> Result<Self, Self::Error> {
        let count_dec = prange
            .count
            .checked_sub(1)
            .with_context(|| {
                format!(
                    "Zero count on a {start_to} {start_from} {count} range",
                    start_to = prange.start_to,
                    start_from = prange.start_from,
                    count = prange.count
                )
            })
            .map_err(Error::Value)?;
        let end_from = prange
            .start_from
            .checked_add(count_dec)
            .with_context(|| {
                format!(
                    "Overflow on a {start_to} {start_from} {count} range",
                    start_to = prange.start_to,
                    start_from = prange.start_from,
                    count = prange.count
                )
            })
            .map_err(Error::Value)?;
        let end_to = prange
            .start_to
            .checked_add(count_dec)
            .with_context(|| {
                format!(
                    "Overflow (end-to) on a {start_to} {start_from} {count} range",
                    start_to = prange.start_to,
                    start_from = prange.start_from,
                    count = prange.count
                )
            })
            .map_err(Error::Value)?;
        Ok(Self {
            start_from: prange.start_from,
            start_to: prange.start_to,
            count: prange.count,
            end_to,
            range: prange.start_from..=end_from,
        })
    }
}

/// A single map from one resource's identifiers to another's.
#[derive(Debug)]
pub struct ResourceMap {
    /// The resource being mapped.
    res_from: String,

    /// The resource being mapped to.
    res_to: String,

    /// The mapping lines themselves.
    ranges: Vec<MapRange>,
}

impl ResourceMap {
    /// The resource being mapped.
    #[inline]
    #[must_use]
    pub fn res_from(&self) -> &str {
        &self.res_from
    }

    /// The resource being mapped to.
    #[inline]
    #[must_use]
    pub fn res_to(&self) -> &str {
        &self.res_to
    }

    /// The mapping lines themselves.
    #[inline]
    #[must_use]
    pub fn ranges(&self) -> &[MapRange] {
        &self.ranges
    }

    /// Split a single range according to our mappings.
    fn split_single_range(&self, range: ResourceRange) -> Result<Vec<ResourceRange>, Error> {
        let (mut res_ranges, done) = self.ranges.iter().try_fold(
            (vec![range], vec![]),
            |(i_ranges, mut done): (Vec<ResourceRange>, Vec<ResourceRange>),
             current|
             -> Result<(Vec<ResourceRange>, Vec<ResourceRange>), Error> {
                let res_ranges = i_ranges
                    .into_iter()
                    .map(|i_range| {
                        let mut res = vec![];
                        if i_range.end() < current.range().start()
                            || i_range.start() > current.range().end()
                        {
                            res.push(i_range);
                        } else if i_range.start() <= current.range().start() {
                            if i_range.start() < current.range().start() {
                                res.push(
                                    *i_range.start()
                                        ..=current
                                            .range()
                                            .start()
                                            .checked_sub(1)
                                            .with_context(|| {
                                                format!("sub 1 from {current:?} for {i_range:?}")
                                            })
                                            .map_err(Error::Process)?,
                                );
                            }
                            if i_range.end() <= current.range().end() {
                                done.push(
                                    current.start_to()
                                        ..=current.remap_in_range(
                                            &self.res_from,
                                            &self.res_to,
                                            *i_range.end(),
                                        )?,
                                );
                            } else {
                                done.push(current.start_to()..=current.end_to());
                                res.push(
                                    (*current.range().end())
                                        .checked_add(1)
                                        .with_context(|| {
                                            format!(
                                                "Could not add 1 to the end of {current:?}",
                                                current = current.range()
                                            )
                                        })
                                        .map_err(Error::Process)?
                                        ..=*i_range.end(),
                                );
                            }
                        } else if i_range.end() <= current.range().end() {
                            done.push(
                                current.remap_in_range(
                                    &self.res_from,
                                    &self.res_to,
                                    *i_range.start(),
                                )?
                                    ..=current.remap_in_range(
                                        &self.res_from,
                                        &self.res_to,
                                        *i_range.end(),
                                    )?,
                            );
                        } else {
                            done.push(
                                current.remap_in_range(
                                    &self.res_from,
                                    &self.res_to,
                                    *i_range.start(),
                                )?..=current.end_to(),
                            );
                            res.push(
                                (*current.range().end())
                                    .checked_add(1)
                                    .with_context(|| {
                                        format!(
                                            "Could not add 1 to the end of {current:?}",
                                            current = current.range()
                                        )
                                    })
                                    .map_err(Error::Process)?
                                    ..=*i_range.end(),
                            );
                        }
                        Ok(res.into_iter())
                    })
                    .flatten_ok()
                    .collect::<Result<Vec<_>, _>>()?;
                Ok((res_ranges, done))
            },
        )?;
        res_ranges.extend(done);
        Ok(res_ranges)
    }

    /// Find the source range a value belongs to (if any), convert it.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] on failure to add and subtract small numbers.
    #[allow(clippy::missing_inline_in_public_items)]
    pub fn split_ranges(
        &self,
        seed_ranges: Vec<ResourceRange>,
    ) -> Result<Vec<ResourceRange>, Error> {
        let mut split = seed_ranges
            .into_iter()
            .map(|range| self.split_single_range(range))
            .flatten_ok()
            .collect::<Result<Vec<_>, _>>()?;
        split.sort_unstable_by(cmp_range_inclusive);
        Ok(split)
    }
}

/// A single map from one resource's identifiers to another's as found in the input data.
#[derive(Debug)]
pub struct ResourceMapParsed {
    /// The resource being mapped.
    res_from: String,

    /// The resource being mapped to.
    res_to: String,

    /// The mapping lines themselves.
    ranges: Vec<MapRangeParsed>,
}

/// All the information about the resources as read from the input file.
#[derive(Debug)]
pub struct Maps {
    /// The initial seeds.
    seeds: Vec<ResourceRange>,

    /// The maps from a resource to another one.
    maps: HashMap<String, ResourceMap>,
}

impl Maps {
    /// The initial seeds.
    #[inline]
    #[must_use]
    pub fn seeds(&self) -> &[ResourceRange] {
        &self.seeds
    }

    /// Split this collection into a seeds vector and a collection of maps only.
    #[inline]
    #[must_use]
    pub fn take_seeds(self) -> (Vec<ResourceRange>, Self) {
        (
            self.seeds,
            Self {
                seeds: vec![],
                maps: self.maps,
            },
        )
    }

    /// Take a value through the maps.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] if an invalid map was specified.
    #[inline]
    pub fn recursive_map(
        &self,
        res_from: &str,
        res_to: &str,
        seed_ranges: Vec<ResourceRange>,
    ) -> Result<Vec<ResourceRange>, Error> {
        let rmap = self
            .maps
            .get(res_from)
            .with_context(|| {
                format!("No map from '{res_from}' to anything defined, wanted one to '{res_to}'")
            })
            .map_err(Error::Process)?;
        let next_ranges = rmap
            .split_ranges(seed_ranges)
            .with_context(|| {
                format!(
                    "Could not look ranges up in the {from}-to-{to} map",
                    from = rmap.res_from(),
                    to = rmap.res_to()
                )
            })
            .map_err(Error::Process)?;
        if rmap.res_to() == res_to {
            Ok(next_ranges)
        } else {
            self.recursive_map(rmap.res_to(), res_to, next_ranges)
        }
    }
}

/// All the information about the resources as read from the input file, raw.
#[derive(Debug)]
pub struct MapsParsed {
    /// The initial seeds.
    seeds: Vec<u32>,

    /// The maps from a resource to another one.
    maps: Vec<ResourceMapParsed>,
}

/// Compare two ranges by their starting point first, then by their end.
#[inline]
#[must_use]
pub fn cmp_range_inclusive(left: &ResourceRange, right: &ResourceRange) -> Ordering {
    left.start()
        .cmp(right.start())
        .then_with(|| left.end().cmp(right.end()))
}

/// Parse the line listing the initial seed indices.
fn p_seeds(input: &str) -> IResult<&str, Vec<u32>> {
    let (r_input, seeds) =
        delimited(tag("seeds:"), many1(preceded(tag(" "), c_u32)), tag("\n"))(input)?;
    Ok((r_input, seeds))
}

/// Parse a map header specifying what this map is for.
fn p_map_header(input: &str) -> IResult<&str, (&str, &str)> {
    let (r_input, (res_from, res_to)) = tuple((
        delimited(tag("\n"), alpha1, tag("-to-")),
        terminated(alpha1, tag(" map:\n")),
    ))(input)?;
    Ok((r_input, (res_from, res_to)))
}

/// Parse a map range line (start, mapped-start, count).
fn p_map_range(input: &str) -> IResult<&str, MapRangeParsed> {
    let (r_input, (start_to, start_from, count)) = tuple((
        c_u32,
        preceded(space1, c_u32),
        delimited(space1, c_u32, tag("\n")),
    ))(input)?;
    Ok((
        r_input,
        MapRangeParsed {
            start_from,
            start_to,
            count,
        },
    ))
}

/// Parse a single map from one kind of resource to another.
fn p_resource_map(input: &str) -> IResult<&str, ResourceMapParsed> {
    let (r_input, (header, ranges)) = tuple((p_map_header, many1(p_map_range)))(input)?;
    Ok((
        r_input,
        ResourceMapParsed {
            res_from: header.0.to_owned(),
            res_to: header.1.to_owned(),
            ranges,
        },
    ))
}

/// Parse the input data: initial seeds, resource maps.
fn p_maps(input: &str) -> IResult<&str, MapsParsed> {
    let (r_input, (seeds, maps)) = all_consuming(tuple((p_seeds, many1(p_resource_map))))(input)?;
    Ok((r_input, MapsParsed { seeds, maps }))
}

/// Read the resource maps in.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
/// [`Error::Value`] if the input file cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn read_maps<C: Config>(cfg: &C, mode: SeedsMode) -> Result<Maps, Error> {
    let contents = fs::read_to_string(cfg.filename())
        .context("Could not read the input file")
        .map_err(|err| Error::Load(format!("{}", cfg.filename().display()), err))?;
    let (_, parsed_maps) = p_maps(&contents)
        .map_err(|err| err.map_input(ToOwned::to_owned))
        .context("Could not parse the input file")
        .map_err(Error::Value)?;

    maps_from_parsed(parsed_maps, mode)
}

/// Convert a simplified parsed structure into a real [`Maps`] object.
///
/// # Errors
///
/// [`Error::Value`] if the input file cannot be parsed.
fn maps_from_parsed(parsed_maps: MapsParsed, mode: SeedsMode) -> Result<Maps, Error> {
    let seeds = match mode {
        SeedsMode::Single => parsed_maps
            .seeds
            .into_iter()
            .map(|seed| seed..=seed)
            .collect(),
        SeedsMode::Ranges => {
            if (parsed_maps.seeds.len() % 2) != 0 {
                return Err(Error::Value(anyhow!("Odd number of seeds for range mode")));
            }
            parsed_maps
                .seeds
                .into_iter()
                .tuples()
                .map(|(seed, count)| {
                    let end = seed
                        .checked_add(
                            count
                                .checked_sub(1)
                                .with_context(|| format!("Zero count for the {seed} initial seed"))
                                .map_err(Error::Value)?,
                        )
                        .with_context(|| {
                            format!(
                                "Could not add the {count} count to the {seed} initial seed value"
                            )
                        })
                        .map_err(Error::Value)?;
                    Ok(seed..=end)
                })
                .collect::<Result<_, _>>()?
        }
    };
    let maps = parsed_maps
        .maps
        .into_iter()
        .map(|pmap| {
            let ranges = pmap
                .ranges
                .into_iter()
                .map(MapRange::try_from)
                .collect::<Result<_, _>>()?;
            Ok((
                pmap.res_from.clone(),
                ResourceMap {
                    res_from: pmap.res_from,
                    res_to: pmap.res_to,
                    ranges,
                },
            ))
        })
        .collect::<Result<HashMap<_, _>, _>>()?;
    Ok(Maps { seeds, maps })
}

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use rstest::rstest;

    use crate::defs::Error;

    use super::{
        MapRange, MapRangeParsed, MapsParsed, ResourceMap, ResourceMapParsed, ResourceRange,
        SeedsMode,
    };

    const MR_SMALL: MapRangeParsed = MapRangeParsed {
        start_to: 10,
        count: 3,
        start_from: 5,
    };

    fn get_sample_map() -> MapsParsed {
        MapsParsed {
            seeds: vec![79, 14, 55, 13],
            maps: vec![
                ResourceMapParsed {
                    res_from: "seed".to_owned(),
                    res_to: "soil".to_owned(),
                    ranges: vec![
                        MapRangeParsed {
                            start_to: 50,
                            start_from: 98,
                            count: 2,
                        },
                        MapRangeParsed {
                            start_to: 52,
                            start_from: 50,
                            count: 48,
                        },
                    ],
                },
                ResourceMapParsed {
                    res_from: "soil".to_owned(),
                    res_to: "fertilizer".to_owned(),
                    ranges: vec![
                        MapRangeParsed {
                            start_to: 0,
                            start_from: 15,
                            count: 37,
                        },
                        MapRangeParsed {
                            start_to: 37,
                            start_from: 52,
                            count: 2,
                        },
                        MapRangeParsed {
                            start_to: 39,
                            start_from: 0,
                            count: 15,
                        },
                    ],
                },
                ResourceMapParsed {
                    res_from: "fertilizer".to_owned(),
                    res_to: "water".to_owned(),
                    ranges: vec![
                        MapRangeParsed {
                            start_to: 49,
                            start_from: 53,
                            count: 8,
                        },
                        MapRangeParsed {
                            start_to: 0,
                            start_from: 11,
                            count: 42,
                        },
                        MapRangeParsed {
                            start_to: 42,
                            start_from: 0,
                            count: 7,
                        },
                        MapRangeParsed {
                            start_to: 57,
                            start_from: 7,
                            count: 4,
                        },
                    ],
                },
            ],
        }
    }

    #[rstest]
    #[case(MR_SMALL, 3)]
    #[case(MR_SMALL, u32::MAX)]
    fn test_remap_in_range_fail(#[case] mrange: MapRangeParsed, #[case] value: u32) {
        let range: MapRange = mrange.try_into().unwrap();
        assert!(matches!(
            range.remap_in_range("from", "to", value),
            Err(Error::Process(_))
        ));
    }

    #[rstest]
    #[case(MR_SMALL, 5, 10)]
    #[case(MR_SMALL, 6, 11)]
    #[case(MR_SMALL, 7, 12)]
    fn test_remap_in_range_ok(
        #[case] mrange: MapRangeParsed,
        #[case] value: u32,
        #[case] exp: u32,
    ) {
        let range: MapRange = mrange.try_into().unwrap();
        assert_eq!(range.remap_in_range("from", "to", value).unwrap(), exp);
    }

    #[rstest]
    #[case(MR_SMALL, 3, 3)]
    #[case(MR_SMALL, 4, 4)]
    #[case(MR_SMALL, 5, 10)]
    #[case(MR_SMALL, 6, 11)]
    #[case(MR_SMALL, 7, 12)]
    #[case(MR_SMALL, 8, 8)]
    #[case(MR_SMALL, 10, 10)]
    #[case(MR_SMALL, u32::MAX, u32::MAX)]
    fn test_remap_ok(#[case] mrange: MapRangeParsed, #[case] value: u32, #[case] exp: u32) {
        let range: MapRange = mrange.try_into().unwrap();
        assert_eq!(range.remap("from", "to", value).unwrap(), exp);
    }

    #[rstest]
    #[case(vec![MR_SMALL], 2..=4, vec![2..=4])]
    #[case(vec![MR_SMALL], 8..=10, vec![8..=10])]
    #[case(vec![MR_SMALL], 5..=5, vec![10..=10])]
    #[case(vec![MR_SMALL], 6..=6, vec![11..=11])]
    #[case(vec![MR_SMALL], 7..=7, vec![12..=12])]
    #[case(vec![MR_SMALL], 2..=5, vec![2..=4, 10..=10])]
    #[case(vec![MR_SMALL], 2..=6, vec![2..=4, 10..=11])]
    #[case(vec![MR_SMALL], 2..=7, vec![2..=4, 10..=12])]
    #[case(vec![MR_SMALL], 2..=13, vec![2..=4, 8..=13, 10..=12])]
    #[case(vec![MR_SMALL], 5..=6, vec![10..=11])]
    #[case(vec![MR_SMALL], 5..=13, vec![8..=13, 10..=12])]
    fn test_split_single_range(
        #[case] pranges: Vec<MapRangeParsed>,
        #[case] trange: ResourceRange,
        #[case] exp: Vec<ResourceRange>,
    ) {
        let map = ResourceMap {
            res_from: "from".to_owned(),
            res_to: "to".to_owned(),
            ranges: pranges
                .into_iter()
                .map(|prange| prange.try_into().unwrap())
                .collect(),
        };
        assert_eq!(map.split_single_range(trange).unwrap(), exp);
    }

    #[rstest]
    #[case("seed", "soil", vec![13..=13, 14..=14, 57..=57, 81..=81])]
    #[case("seed", "fertilizer", vec![52..=52, 53..=53, 57..=57, 81..=81])]
    #[case("seed", "water", vec![41..=41, 49..=49, 53..=53, 81..=81])]
    fn test_recursive_map(
        #[case] res_from: &str,
        #[case] res_to: &str,
        #[case] exp: Vec<ResourceRange>,
    ) {
        let maps = super::maps_from_parsed(get_sample_map(), SeedsMode::Single).unwrap();
        assert_eq!(maps.maps.len(), 3);
        assert_eq!(
            maps.recursive_map(res_from, res_to, maps.seeds.clone())
                .unwrap(),
            exp
        );
    }
}
