// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Aim that trebuchet.

use anyhow::{anyhow, Context};

use crate::defs::{BuildDecimal, Error};

/// The sequences that we recognize as digits
const DIGIT_NAMES: [(&str, u32); 10] = [
    ("zero", 0),
    ("one", 1),
    ("two", 2),
    ("three", 3),
    ("four", 4),
    ("five", 5),
    ("six", 6),
    ("seven", 7),
    ("eight", 8),
    ("nine", 9),
];

/// Look for a string that looks like a digit at the start of the given substring.
fn parse_starting_digit(line: &str) -> Option<u32> {
    DIGIT_NAMES
        .iter()
        .find_map(|&(digit_str, value)| line.starts_with(digit_str).then_some(value))
}

/// Parse a single calibration value from a line of text that may or may not contain words.
///
/// # Errors
///
/// [`Error::Value`] if there is no digit at all on this line.
/// [`Error::Process`] if something goes wrong with multiplying or adding small numbers.
#[allow(clippy::missing_inline_in_public_items)]
pub fn parse_calibration(line: &str, longform: bool) -> Result<u32, Error> {
    let (first, last) = line
        .char_indices()
        .try_fold(None, |acc, (idx, chr)| {
            let digit = {
                let digit_triv = chr.to_digit(10);
                if longform && digit_triv.is_none() {
                    let sub = line
                        .get(idx..)
                        .with_context(|| {
                            format!("String slicing: chr {chr:?} idx {idx:?} line {line:?}")
                        })
                        .map_err(Error::Internal)?;
                    parse_starting_digit(sub)
                } else {
                    digit_triv
                }
            };

            match (digit, acc) {
                (None, current) => Ok(current),
                (Some(first), None) => Ok(Some((first, first))),
                (Some(last), Some((first, _))) => Ok(Some((first, last))),
            }
        })?
        .with_context(|| format!("No digits in the {line:?} line"))
        .map_err(Error::Value)?;

    if first > 9 || last > 9 {
        return Err(Error::Internal(anyhow!(
            "Parsing {line:?} returned first digit {first:?} and last digit {last:?}"
        )));
    }
    first.add_digit(last)
}

#[cfg(test)]
#[allow(clippy::panic_in_result_fn)]
mod tests {
    use anyhow::{Context, Error as AnyError};
    use rstest::rstest;

    use crate::defs::Error;

    #[rstest]
    #[case("")]
    #[case("hello")]
    #[case(" and stuff!")]
    #[case("eigh")]
    #[case("twO")]
    #[case("eight")]
    #[case("seven and two")]
    #[case("This one is not better than those three, is it now?")]
    #[case("And here is an eightwo case")]
    fn test_parse_short_fail(#[case] line: &str) {
        assert!(matches!(
            super::parse_calibration(line, false),
            Err(Error::Value(_))
        ));
    }

    #[rstest]
    #[case("1", 11)]
    #[case("12", 12)]
    #[case("a3", 33)]
    #[case("a34", 34)]
    #[case("4b", 44)]
    #[case("45b", 45)]
    #[case("something 9 weird is going 8 on 7 here 6!", 96)]
    #[case("Forty-two, not 43, or as we like to call it, 24 backwards", 44)]
    fn test_parse_short_ok(#[case] line: &str, #[case] exp: u32) -> Result<(), AnyError> {
        assert_eq!(
            super::parse_calibration(line, false).with_context(|| "Could not parse {line:?}")?,
            exp
        );
        Ok(())
    }

    #[rstest]
    #[case("")]
    #[case("hello")]
    #[case(" and stuff!")]
    #[case("eigh")]
    #[case("twO")]
    fn test_parse_long_fail(#[case] line: &str) {
        assert!(matches!(
            super::parse_calibration(line, true),
            Err(Error::Value(_))
        ));
    }

    #[rstest]
    #[case("1", 11)]
    #[case("12", 12)]
    #[case("a3", 33)]
    #[case("a34", 34)]
    #[case("4b", 44)]
    #[case("45b", 45)]
    #[case("something 9 weird is going 8 on 7 here 6!", 96)]
    #[case("eight", 88)]
    #[case("seven and two", 72)]
    #[case("This one is not better than those three, is it now?", 13)]
    #[case("Forty-two, not 43, or as we like to call it, 24 backwards", 24)]
    #[case("And here is an eightwo case", 82)]
    fn test_parse_long_ok(#[case] line: &str, #[case] exp: u32) -> Result<(), AnyError> {
        assert_eq!(
            super::parse_calibration(line, true).with_context(|| "Could not parse {line:?}")?,
            exp
        );
        Ok(())
    }
}
