// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Play with some red, green, and blue cubes.

use std::borrow::ToOwned;
use std::cmp::max;
use std::collections::HashMap;

use anyhow::Context;
use nom::{
    branch::alt,
    bytes::complete::tag,
    character::complete::u32 as c_u32,
    combinator::all_consuming,
    multi::separated_list0,
    sequence::{delimited, terminated, tuple},
    IResult,
};

use crate::defs::{Config, Error};

/// A single draw within a game.
#[derive(Debug, Clone, Copy)]
pub struct Draw {
    /// The count of the red cubes revealed in the draw.
    red: u32,

    /// The count of the green cubes revealed in the draw.
    green: u32,

    /// The count of the blue cubes revealed in the draw.
    blue: u32,
}

impl Draw {
    /// The count of the red cubes revealed in the draw.
    #[inline]
    #[must_use]
    pub const fn red(&self) -> u32 {
        self.red
    }

    /// The count of the green cubes revealed in the draw.
    #[inline]
    #[must_use]
    pub const fn green(&self) -> u32 {
        self.green
    }

    /// The count of the blue cubes revealed in the draw.
    #[inline]
    #[must_use]
    pub const fn blue(&self) -> u32 {
        self.blue
    }

    /// Construct a [`Draw`] object from the cube counts.
    #[inline]
    #[must_use]
    pub const fn from_rgb(red: u32, green: u32, blue: u32) -> Self {
        Self { red, green, blue }
    }

    /// Compare the draw against the specified threshold.
    #[inline]
    #[must_use]
    pub const fn possible(&self, thresh: &Self) -> bool {
        self.red <= thresh.red && self.green <= thresh.green && self.blue <= thresh.blue
    }

    /// Get the covering max draw.
    #[inline]
    #[must_use]
    fn max_of(&self, other: &Self) -> Self {
        Self {
            red: max(self.red, other.red),
            green: max(self.green, other.green),
            blue: max(self.blue, other.blue),
        }
    }

    /// Get the power of this draw.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] on failure to add or multiply small numbers.
    #[inline]
    pub fn power(&self) -> Result<u32, Error> {
        self.red
            .checked_mul(self.green)
            .with_context(|| {
                format!(
                    "Could not multiply {red} by {green}",
                    red = self.red,
                    green = self.green
                )
            })
            .map_err(Error::Process)?
            .checked_mul(self.blue)
            .with_context(|| {
                format!(
                    "Could not multiply {red} by {green} by {blue}",
                    red = self.red,
                    green = self.green,
                    blue = self.blue
                )
            })
            .map_err(Error::Process)
    }
}

/// A single game to be analyzed.
#[derive(Debug)]
pub struct Game {
    /// The sequence number of the game.
    id: u32,

    /// The draws made in the game.
    draws: Vec<Draw>,
}

impl Game {
    /// The sequence number of the game.
    #[inline]
    #[must_use]
    pub const fn id(&self) -> u32 {
        self.id
    }

    /// The draws made in the game.
    #[inline]
    #[must_use]
    pub fn draws(&self) -> &[Draw] {
        &self.draws
    }

    /// Construct a [`Game`] object from a series of draws.
    #[inline]
    #[must_use]
    fn new(id: u32, draws: Vec<Draw>) -> Self {
        Self { id, draws }
    }

    /// Calculate the max covering draw and return its power.
    ///
    /// # Errors
    ///
    /// [`Error::Process`] on failure to add or multiply small numbers.
    #[inline]
    pub fn power(&self) -> Result<u32, Error> {
        match self.draws.split_first() {
            None => Ok(0),
            Some((first, rest)) => rest
                .iter()
                .fold(*first, |cover, draw| cover.max_of(draw))
                .power(),
        }
    }
}

/// Parse a single draw (red, green, blue in any order).
fn p_draw(input: &str) -> IResult<&str, Draw> {
    let (r_input, parts) = separated_list0(
        tag(", "),
        tuple((
            terminated(c_u32, tag(" ")),
            alt((tag("red"), tag("green"), tag("blue"))),
        )),
    )(input)?;
    let res = parts
        .into_iter()
        .map(|(count, color)| (color, count))
        .collect::<HashMap<_, _>>();
    Ok((
        r_input,
        Draw::from_rgb(
            *res.get("red").unwrap_or(&0),
            *res.get("green").unwrap_or(&0),
            *res.get("blue").unwrap_or(&0),
        ),
    ))
}

/// Parse a full game description line.
fn p_game_line(input: &str) -> IResult<&str, Game> {
    let (r_input, (id, draws)) = all_consuming(tuple((
        delimited(tag("Game "), c_u32, tag(": ")),
        separated_list0(tag("; "), p_draw),
    )))(input)?;
    Ok((r_input, Game::new(id, draws)))
}

/// Read in a list of games from a file.
///
/// # Errors
///
/// [`Error::Load`] if the input file cannot be read.
/// [`Error::Value`] if an input line cannot be parsed.
#[allow(clippy::missing_inline_in_public_items)]
pub fn read_games<C: Config>(cfg: &C) -> Result<Vec<Game>, Error> {
    cfg.read_lines()?
        .map(|line_res| -> Result<Game, Error> {
            let line = line_res?;
            let (_, game) = p_game_line(&line)
                .map_err(|err| err.map_input(ToOwned::to_owned))
                .with_context(|| format!("Could not parse the {line:?} input line"))
                .map_err(Error::Value)?;
            Ok(game)
        })
        .collect()
}

/// The counts to compare the games against.
pub const DRAW_THRESHOLD: Draw = Draw::from_rgb(12, 13, 14);

#[cfg(test)]
#[allow(clippy::unwrap_used)]
mod tests {
    use rstest::rstest;

    use super::{Draw, Game, DRAW_THRESHOLD};

    #[rstest]
    #[case(1, 2, 3, true)]
    #[case(6, 5, 4, true)]
    #[case(7, 8, 9, true)]
    #[case(12, 11, 10, true)]
    #[case(12, 13, 14, true)]
    #[case(12, 14, 13, false)]
    #[case(14, 13, 14, false)]
    fn test_possible(#[case] red: u32, #[case] green: u32, #[case] blue: u32, #[case] exp: bool) {
        assert_eq!(
            Draw::from_rgb(red, green, blue).possible(&DRAW_THRESHOLD),
            exp
        );
    }

    #[rstest]
    #[case((1, 2, 3), (4, 0, 2), (4, 2, 3), 24)]
    #[case((0, 5, 10), (7, 2, 15), (7, 5, 15), 525)]
    #[case((4, 0, 3), (1, 2, 6), (4, 2, 6), 48)]
    #[case((4, 2, 6), (0, 2, 0), (4, 2, 6), 48)]
    #[case((6, 3, 1), (1, 2, 2), (6, 3, 2), 36)]
    fn test_draw_power(
        #[case] first: (u32, u32, u32),
        #[case] second: (u32, u32, u32),
        #[case] res: (u32, u32, u32),
        #[case] exp: u32,
    ) {
        let d_first = Draw::from_rgb(first.0, first.1, first.2);
        let d_second = Draw::from_rgb(second.0, second.1, second.2);
        let d_res = d_first.max_of(&d_second);
        assert_eq!((d_res.red(), d_res.green(), d_res.blue()), res);
        assert_eq!(d_res.power().unwrap(), exp);
    }

    #[rstest]
    #[case((1, 2, 3), (4, 0, 2), 24)]
    #[case((0, 5, 10), (7, 2, 15), 525)]
    #[case((4, 0, 3), (1, 2, 6), 48)]
    #[case((4, 2, 6), (0, 2, 0), 48)]
    #[case((6, 3, 1), (1, 2, 2), 36)]
    fn test_game_power(
        #[case] first: (u32, u32, u32),
        #[case] second: (u32, u32, u32),
        #[case] exp: u32,
    ) {
        let game = Game::new(
            616,
            vec![
                Draw::from_rgb(first.0, first.1, first.2),
                Draw::from_rgb(second.0, second.1, second.2),
            ],
        );
        assert_eq!(game.power().unwrap(), exp);
    }
}
